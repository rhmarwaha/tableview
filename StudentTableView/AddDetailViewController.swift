//
//  AddDetailViewController.swift
//  StudentTableView
//
//  Created by Rohit Marwaha on 23/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class AddDetailViewController: UIViewController {
    
    @IBOutlet weak var lastname: UITextField!
    @IBOutlet weak var firstname: UITextField!
    @IBOutlet weak var departmentSegement: UISegmentedControl!
    @IBOutlet weak var saveButton: UIButton!
    
    
   
    @IBOutlet weak var dob: UITextField!
    @IBOutlet weak var age: UILabel!
    @IBOutlet weak var country: UITextField!
    
    
    var addUserDelegate:  AddUserDelegate!
    
    var departmentValue: String!
    
    let datePicker = UIDatePicker()
    let pickerView = UIPickerView()
    
    let countries = ["India" , "Pakistan" , "USA" , "Germany", "Australia"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        
        datePicker.datePickerMode = .date
        dob.inputView = datePicker
        datePicker.minimumDate = Calendar.current.date(byAdding: Calendar.Component.year, value: -60, to: Date())
        datePicker.maximumDate = Calendar.current.date(byAdding: Calendar.Component.year , value: -13, to: Date())
        datePicker.addTarget(self, action: #selector(returnDataValue), for: UIControl.Event.valueChanged)
        
        
        country.inputView = pickerView
        pickerView.delegate = self
        pickerView.dataSource = self
        
        departmentValue = departmentSegement.titleForSegment(at: departmentSegement.selectedSegmentIndex)
        
        saveButton.layer.cornerRadius = saveButton.bounds.height / 2 - 5
        departmentSegement.selectedSegmentIndex = 0
    }
    
    @IBAction func cancelBarButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func department(_ sender: Any) {
        
        departmentValue = departmentSegement.titleForSegment(at: departmentSegement.selectedSegmentIndex)
        
        // departmentValue = Sections.returnTag(section: departmentSegement.selectedSegmentIndex)
    }
    
    @objc func returnDataValue(){
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dob.text = dateFormatter.string(from: datePicker.date)
        let calender = Calendar.current
        let components = calender.dateComponents([.year], from: datePicker.date, to: Date())
        age.text = String(components.year!)
        
    }
    
    
    @IBAction func saveButton(_ sender: UIButton) {
        
        let first = firstname.text
        let last = lastname.text
        let studentAge = age.text ?? ""
        let studentDob = dob.text ?? ""
        let studentCountry = country.text ?? ""
        
        
        if  first!.isEmpty || last!.isEmpty || age.text!.isEmpty || dob.text!.isEmpty || country.text!.isEmpty{         
            let alertController = UIAlertController(title: "Details", message: "Add proper details", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default) { (_) in
                alertController.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(okAction)
            self.present(alertController,animated: true)
            
        }else{
            
        let studentName = (firstname.text ?? "") + " " + (lastname.text ?? "")
        let image = UIImage(named: "download")!
        
       
            
        let studentDetail = StudentDetails(studentName: studentName, studentImage: image, studentRegistrationNumber: "", studentSection: departmentValue,studentDob: studentDob,studentAge: studentAge,studentCountry: studentCountry)

        addUserDelegate.addUserData(studentDetail: studentDetail)
        self.dismiss(animated: true, completion: nil)
        
    }
 }
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.dob.resignFirstResponder()
        self.country.resignFirstResponder()
        
    }
}

extension AddDetailViewController: UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return countries.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return countries[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        country.text = countries[row]
    }
}
