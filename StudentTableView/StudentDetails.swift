//
//  StudentDetails.swift
//  StudentTableView
//
//  Created by Rohit Marwaha on 20/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import Foundation
import UIKit


class Department{
    var departmentName: String
    var studentDetails: [StudentDetails]
    init(departmentName: String, studentDetails: [StudentDetails]) {
        self.departmentName = departmentName
        self.studentDetails = studentDetails
    }
}

class StudentDetails {
    var studentName: String
    var studentImage: UIImage!
    var studentRegistrationNumber: String
    var studentSection: String
    var studentDob: String
    var studentAge: String
    var studentCountry: String
    
    
    init(studentName: String , studentImage: UIImage , studentRegistrationNumber: String , studentSection: String, studentDob: String,  studentAge: String, studentCountry: String) {
        self.studentName = studentName
        self.studentImage = studentImage
        self.studentSection = studentSection
        self.studentRegistrationNumber = studentRegistrationNumber
        self.studentDob = studentDob
        self.studentAge = studentAge
        self.studentCountry = studentCountry
    }
}

