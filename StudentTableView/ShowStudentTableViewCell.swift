//
//  ShowStudentTableViewCell.swift
//  StudentTableView
//
//  Created by Rohit Marwaha on 24/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class ShowStudentTableViewCell: UITableViewCell {

    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var answerLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
