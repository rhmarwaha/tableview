//
//  LoginViewController.swift
//  StudentTableView
//
//  Created by Rohit Marwaha on 27/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

let nameKey = "name"
let passwordKey = "password"
let rememberMeKey = "rememberme"


class LoginViewController: UIViewController {

    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var registerNowLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var rememberMeValue: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        self.navigationItem.title = "Login"
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(register))
        registerNowLabel.isUserInteractionEnabled = true
        registerNowLabel.addGestureRecognizer(tapRecognizer)
  
    }
    
    @objc func register(){
        if let signUpViewController = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController {
            self.navigationController?.pushViewController(signUpViewController, animated: true)
        }
    }
    
    
    @IBAction func rememberMeAction(_ sender: UISwitch) {
        if sender.isOn{
            rememberMeValue = true
        }else{
            rememberMeValue = false
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.hidesBackButton = true
    }
    @IBAction func loginButtonAction(_ sender: Any) {
    
        let email = emailTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        
        guard let emailUserDefaults = UserDefaults.standard.string(forKey: nameKey) else {
            return
        }
        
        guard let passwordUserDefaults = UserDefaults.standard.string(forKey: passwordKey) else {
            return
        }
        UserDefaults.standard.set(rememberMeValue, forKey: rememberMeKey)
        print(emailUserDefaults)
        if  email.elementsEqual(emailUserDefaults) || password.elementsEqual(passwordUserDefaults) {
            if let viewController = self.storyboard?.instantiateViewController(withIdentifier: "ViewController") as? ViewController{
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }else {
            let alertController = UIAlertController(title: "Authentication Warning", message: "Wrong Details", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default) { (_) in
                alertController.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(okAction)
            self.present(alertController,animated: true)
        }
    
    }
    
}
