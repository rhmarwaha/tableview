//
//  headerView.swift
//  StudentTableView
//
//  Created by Rohit Marwaha on 23/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import Foundation
import UIKit
class HeaderView: UITableViewHeaderFooterView{
    
    @IBOutlet weak var headerLabel: UILabel!
    
}
