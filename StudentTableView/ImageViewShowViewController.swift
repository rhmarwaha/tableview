//
//  ImageViewShowViewController.swift
//  StudentTableView
//
//  Created by Rohit Marwaha on 20/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class ImageViewShowViewController: UIViewController {

    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    var image: UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imageView.image = image
       
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(viewTargetted))
        parentView.isUserInteractionEnabled = true
        parentView.addGestureRecognizer(tapRecognizer)
        
    
    }

    @objc func viewTargetted(){
        self.dismiss(animated: true, completion: nil)
    }

}
