//
//  SignUpViewController.swift
//  StudentTableView
//
//  Created by Rohit Marwaha on 27/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var firstnameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    
    var terms = false
    override func viewDidLoad() {
        super.viewDidLoad()
       
    }
    @IBAction func termsAndConditionAction(_ sender: UISwitch) {
        if sender.isOn{
            terms = true
        }else{
            terms = false
        }
    }
    @IBAction func signUpButton(_ sender: Any) {
        
        let firstName = firstnameTextField.text ?? ""
        if firstName.isEmpty{
            alertingControllers(title: "Details Warning", message: "Plz fill first name....")
            return
        }
        let lastName = lastNameTextField.text ?? ""
        if lastName.isEmpty{
            alertingControllers(title: "Details Warning", message: "Plz fill last name....")
            return
        }
        let email = emailTextField.text ?? ""
        if email.isEmpty{
            alertingControllers(title: "Details Warning ", message: "Plz fill email....")
            return
        }
        let password = passwordTextField.text ?? ""
        if password.isEmpty{
            alertingControllers(title: "Details Warning", message: "Plz fill password ....")
            return
        }
        let confirmPassword = confirmPasswordTextField.text ?? ""
        if confirmPassword.isEmpty{
            alertingControllers(title: "Details Warning", message: "Plz fill confirm password ....")
            return
        }else{
            if password.elementsEqual(confirmPassword){
                UserDefaults.standard.set(email, forKey: nameKey)
                UserDefaults.standard.set(password, forKey: passwordKey)
                if !terms{
                    alertingControllers(title: "Terms And Conditions", message: "Plz Accept Terms and Conditions")
                    return
                }
                self.navigationController?.popViewController(animated: true)
            }
            else{
                alertingControllers(title: "Password Warning", message: "Password mismatch........")
                return
                }
       }
    }

    func alertingControllers(title: String, message: String){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default) { (_) in
            alertController.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        self.present(alertController, animated: true)
    }
}


