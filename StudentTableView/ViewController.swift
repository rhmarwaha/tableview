//
//  ViewController.swift
//  StudentTableView
//
//  Created by Rohit Marwaha on 20/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit




class ViewController: UIViewController {
    
   // var students = [[StudentDetails]]()
      let departments = [Department(departmentName: "IOS", studentDetails: []),Department(departmentName: "Android", studentDetails: []),Department(departmentName: "Backend", studentDetails: []),Department(departmentName: "Frontend", studentDetails: []),Department(departmentName: "Solutions", studentDetails: []),Department(departmentName: "Others", studentDetails: [])]
    
    
   // @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Delta School"
        
        let studentCellNibView = UINib(nibName: "StudentTableViewCell", bundle: Bundle.main)
        print(studentCellNibView)
       // let studentCellNibView = UINib(nibName: "StudentTableViewCell", bundle: Bundle.main)
        self.tableView.register(studentCellNibView, forCellReuseIdentifier: "StudentTableViewCell")
        //tableView.register(studentCellNibView, forCellReuseIdentifier: "StudentTableViewCell")
        
        let emptyDataCellNibView = UINib(nibName: "EmptyDataTableViewCell", bundle: Bundle.main)
        tableView.register(emptyDataCellNibView, forCellReuseIdentifier: "EmptyDataTableViewCell")
        
        let headerNibView = UINib(nibName: "HeaderView", bundle: Bundle.main)
        tableView.register(headerNibView, forHeaderFooterViewReuseIdentifier: "HeaderView")
        
        
        tableView.dataSource = self
        tableView.delegate = self
        
        loadStudentDetails()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.tableView.reloadData()
        self.navigationItem.hidesBackButton = true
    }

    
    @IBAction func logoutBarItemButton(_ sender: Any) {
        UserDefaults.standard.removeObject(forKey: rememberMeKey)
        if let arrayViewControllers = self.navigationController?.viewControllers {
            for controller in arrayViewControllers {
                if controller is LoginViewController {
                    self.navigationController?.popToViewController(controller, animated: true)
                    return
                }
            }
        }
        
        if let loginViewController = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController {
            self.navigationController?.pushViewController(loginViewController, animated: true)
        }
    }
    
    @IBAction func addBarButton(_ sender: Any) {
        
        if let addDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "AddDetailViewController") as? AddDetailViewController {
            addDetailViewController.addUserDelegate = self
            self.present(addDetailViewController,animated: true)
            
        }
    }
    
    
    func loadStudentDetails(){
        
        
      

        // IOS Students
        let iosStudentDetail1 = StudentDetails(studentName: "Rohit" , studentImage: UIImage(named: "baby1")!, studentRegistrationNumber: "IOS1", studentSection: departments[0].departmentName, studentDob: "22-05-1991", studentAge: "28",studentCountry: "India" )
        let iosStudentDetail2 = StudentDetails(studentName: "Shivam", studentImage: UIImage(named: "baby2")! , studentRegistrationNumber: "IOS2", studentSection: departments[0].departmentName, studentDob: "22-05-1991", studentAge: "28",studentCountry: "Australia")
        
        
        departments[0].studentDetails.append(contentsOf: [iosStudentDetail1,iosStudentDetail2])
        
        
        // Android Students
        let androidStudentDetail1 = StudentDetails(studentName: "Rohit" , studentImage: UIImage(named: "baby3")!, studentRegistrationNumber: "Android1", studentSection: departments[1].departmentName, studentDob: "22-05-1991", studentAge: "28",studentCountry: "India")
        let androidStudentDetail2 = StudentDetails(studentName: "Shivam", studentImage: UIImage(named: "baby4")! , studentRegistrationNumber: "Android2", studentSection: departments[1].departmentName, studentDob: "22-05-1991", studentAge: "28",studentCountry: "Australia")
        let androidStudentDetail3 = StudentDetails(studentName: "Raj", studentImage: UIImage(named: "baby5")! , studentRegistrationNumber: "Android3", studentSection: departments[1].departmentName, studentDob: "22-05-1991", studentAge: "28",studentCountry: "India")
        
        departments[1].studentDetails.append(contentsOf: [androidStudentDetail1,androidStudentDetail2,androidStudentDetail3])
        
        let backendStudentDetail1 = StudentDetails(studentName: "Rishabh" , studentImage: UIImage(named: "baby6")!, studentRegistrationNumber: "Backend1", studentSection: departments[2].departmentName, studentDob: "22-05-1991", studentAge: "28",studentCountry: "Australia")
        let backendStudentDetail2 = StudentDetails(studentName: "Shivam", studentImage: UIImage(named: "baby7")! , studentRegistrationNumber: "Backend2", studentSection: departments[2].departmentName, studentDob: "22-05-1991", studentAge: "",studentCountry: "India")
        let backendStudentDetail3 = StudentDetails(studentName: "Raj", studentImage: UIImage(named: "download")! , studentRegistrationNumber: "Backend3", studentSection: departments[2].departmentName, studentDob: "22-05-1991", studentAge: "28",studentCountry: "Australia")
         departments[2].studentDetails.append(contentsOf: [backendStudentDetail1,backendStudentDetail2,backendStudentDetail3])
        
        
        
        let frontendStudentDetail1 = StudentDetails(studentName: "Shahnwaz", studentImage: UIImage(named: "download")!, studentRegistrationNumber: "Frontend1", studentSection: departments[3].departmentName, studentDob: "22-05-1991", studentAge: "28",studentCountry: "India")
        departments[3].studentDetails.append(contentsOf: [frontendStudentDetail1])
        
        
        let solutionsStudentDetail1 = StudentDetails(studentName: "PK", studentImage: UIImage(named: "download")!, studentRegistrationNumber: "Solutions1", studentSection: departments[4].departmentName, studentDob: "22-05-1991", studentAge: "28",studentCountry: "India")
        departments[4].studentDetails.append(contentsOf: [solutionsStudentDetail1])
        
        let otherStudentdetail1 = StudentDetails(studentName: "Ravi", studentImage: UIImage(named: "download")!, studentRegistrationNumber: "Other1", studentSection: departments[5].departmentName, studentDob: "22-05-1991", studentAge: "28",studentCountry: "Australia")
        departments[5].studentDetails.append(contentsOf: [otherStudentdetail1])
        
    }
    
}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return departments.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if departments[section].studentDetails.isEmpty{
            return 1
        }
        return departments[section].studentDetails.count

//        if students[section].isEmpty {
//            return 1
//        }
//        return students[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      // print("\(students[indexPath.section]) ----  \(students[indexPath.section].isEmpty) ---- \(indexPath.row)")

        
        
        if departments[indexPath.section].studentDetails.isEmpty{
            if let emptyDataCell = tableView.dequeueReusableCell(withIdentifier: "EmptyDataTableViewCell",for: indexPath) as? EmptyDataTableViewCell {
                
                return emptyDataCell
        }
        }else{
            guard let cell: StudentTableViewCell = tableView.dequeueReusableCell(withIdentifier: "StudentTableViewCell", for: indexPath) as? StudentTableViewCell else {
                fatalError("")
            }

   

       
        
        let studentDetail = departments[indexPath.section].studentDetails[indexPath.row]
        //let studentDetail = students[indexPath.section][indexPath.row]
        cell.studentName.text = studentDetail.studentName
        cell.studentImage.image = studentDetail.studentImage
        cell.studentRegisterationNumber.text = "REG NO.  " + studentDetail.studentRegistrationNumber
        
        
        
        cell.buttontappedDelegate = self
        cell.studentImageTapped = studentDetail.studentImage
        
        cell.accessoryType = .disclosureIndicator
        
        return cell
        }
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if departments[indexPath.section].studentDetails.isEmpty {
            return
        }
        print("\(indexPath.section)   \(indexPath.row) ")
        if let showStudentDetailViewController = self.storyboard?.instantiateViewController(withIdentifier: "ShowStudentDetailViewController")  as? ShowStudentDetailViewController{
            showStudentDetailViewController.studentDetail = departments[indexPath.section].studentDetails[indexPath.row]
            self.navigationController?.pushViewController(showStudentDetailViewController, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let headerView: HeaderView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as? HeaderView else{
            fatalError("")
        }
        
        //headerView.headerLabel.text = Sections.returnTag(section: section).rawValue
        headerView.headerLabel.text = departments[section].departmentName
        return headerView
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete{
            
           departments[indexPath.section].studentDetails.remove(at: indexPath.row)
           
            if departments[indexPath.section].studentDetails.isEmpty {
                tableView.reloadData()
            } else {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if departments[indexPath.section].studentDetails.isEmpty {
           return false
        } else {
           return true
        }
    }
}


protocol ButtonTappedDelegate {
    func buttonTapped( studentImage: UIImage)
}


protocol AddUserDelegate{
    func addUserData(studentDetail: StudentDetails)
}

extension ViewController: ButtonTappedDelegate , AddUserDelegate {
    
    func addUserData(studentDetail: StudentDetails) {
        
        var counter = 0
        for i in departments{
            if studentDetail.studentSection.elementsEqual(i.departmentName){
              break
            }
            counter += 1
        }
        
        let registrationNumber = studentDetail.studentSection + String( departments[counter].studentDetails.count + 1)
        studentDetail.studentRegistrationNumber = registrationNumber
        departments[counter].studentDetails.append(studentDetail)

    
    }
    
    func buttonTapped( studentImage: UIImage) {
        
        let imageViewShowViewController = ImageViewShowViewController(nibName: "ImageViewShowViewController", bundle: Bundle.main)
        imageViewShowViewController.image = studentImage
        imageViewShowViewController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(imageViewShowViewController,animated: true)
        
    }
    
    
}
