//
//  EmptyDataTableViewCell.swift
//  StudentTableView
//
//  Created by Rohit Marwaha on 24/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class EmptyDataTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
