//
//  ShowStudentDetailViewController.swift
//  StudentTableView
//
//  Created by Rohit Marwaha on 24/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

enum  StudentDetailEnum: String{
    case Name
    case Age
    case Dob
    case Country
    
}

extension StudentDetailEnum{
    static func returnEnumValue(studentDetailRow: Int) -> StudentDetailEnum{
        switch studentDetailRow{
        case 0:
            return StudentDetailEnum.Name
        case 1:
            return StudentDetailEnum.Age
        case 2:
            return StudentDetailEnum.Dob
        case 3:
            return StudentDetailEnum.Country
        default:
            return StudentDetailEnum.Name
        }
    }
    static func returnStudentDetailValue(studentDetailRow: Int , studentDetail: StudentDetails) -> String{
        switch studentDetailRow{
        case 0:
            return studentDetail.studentName
        case 1:
            return studentDetail.studentAge
        case 2:
            return studentDetail.studentDob
        case 3:
            return studentDetail.studentCountry
        default:
            return studentDetail.studentName
        }
    }
}

class ShowStudentDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    weak var studentDetail: StudentDetails!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = studentDetail.studentSection
        
        let showStudentTableViewCell = UINib(nibName: "ShowStudentTableViewCell", bundle: Bundle.main)
        tableView.register(showStudentTableViewCell, forCellReuseIdentifier: "ShowStudentTableViewCell")
        tableView.dataSource = self
        tableView.delegate = self

    }
    
}
 



extension ShowStudentDetailViewController: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ShowStudentTableViewCell") as? ShowStudentTableViewCell else {
            fatalError("")
        }
        cell.detailLabel.text = StudentDetailEnum.returnEnumValue(studentDetailRow: indexPath.row).rawValue
        cell.answerLabel.text = StudentDetailEnum.returnStudentDetailValue(studentDetailRow: indexPath.row, studentDetail: studentDetail)
        return cell
    }


}
