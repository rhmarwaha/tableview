//
//  ShowDetailViewController.swift
//  StudentTableView
//
//  Created by Rohit Marwaha on 20/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class ShowDetailViewController: UIViewController {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageLabel: UIImageView!
    @IBOutlet weak var regLabel: UILabel!
  
    var studentDetail: StudentDetails!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        nameLabel.text = studentDetail.studentName
        imageLabel.image = studentDetail.studentImage
        regLabel.text = studentDetail.studentRegistrationNumber
         
        
    }
    
}
