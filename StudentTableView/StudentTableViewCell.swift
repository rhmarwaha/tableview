//
//  StudentTableViewCell.swift
//  StudentTableView
//
//  Created by Rohit Marwaha on 20/09/19.
//  Copyright © 2019 Rohit Marwaha. All rights reserved.
//

import UIKit

class StudentTableViewCell: UITableViewCell {

    @IBOutlet weak var studentImage: UIImageView!
    @IBOutlet weak var studentName: UILabel!
    @IBOutlet weak var studentRegisterationNumber: UILabel!
    
    
    var buttontappedDelegate: ButtonTappedDelegate!
    
    var studentImageTapped: UIImage!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
//        if studentImage.bounds.width > studentImage.bounds.height {
//            studentImage.layer.cornerRadius = studentImage.bounds.height / 2
//     }else{
            studentImage.layer.cornerRadius = 35.0
        
    //  }
        
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageViewTapped))
        studentImage.isUserInteractionEnabled = true
        studentImage.addGestureRecognizer(tapRecognizer)
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @objc func imageViewTapped(){
        self.buttontappedDelegate.buttonTapped(studentImage: studentImageTapped)
    }
    
}



